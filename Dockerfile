FROM python:3.8.0-buster

COPY main.py /usr/local/bin/trello
COPY requirements.txt /requirements.txt

RUN chmod +x /usr/local/bin/trello \
    && pip install -r /requirements.txt

WORKDIR /trello

ENTRYPOINT ["/usr/local/bin/trello"]