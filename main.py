#!/usr/bin/env python3

import argparse
import requests
import os
import logging
import json
import sys


class TrelloApi:
    """Base class for all trello api objects"""

    API_URL = 'https://api.trello.com/1'

    def __init__(self):
        self._api_key = os.getenv('API_KEY', None)
        if self._api_key is None:
            raise ValueError('API_KEY environment variable cannot be empty')

        self._api_token = os.getenv('API_TOKEN', None)
        if self._api_token is None:
            raise ValueError('API_TOKEN environment variable cannot be empty')

    def _add_auth(self, params):
        params['key'] = self._api_key
        params['token'] = self._api_token

    def get(self, url, params):
        r"""Get a requested resource.

        :param url: URL for the get request.
        :param params: Dictionary to send as the query string.
        :return: response of the get request
        :rtype: Dictionary
        """

        self._add_auth(params)
        headers = {'Accept': 'application/json'}

        r = requests.get(url, params=params, headers=headers)
        r.raise_for_status()
        return json.loads(r.text)

    def add(self, url, data):
        r"""Add a new resource

        :param url: URL for the get request.
        :param params: Dictionary to submit as body of the request.
        :return: response of the request
        :rtype: Dictionary
        """

        params = {}
        self._add_auth(params)
        headers = {'Content-Type': 'application/json; charset=utf-8'}

        r = requests.post(url, params=params, data=data)
        r.raise_for_status()
        return json.loads(r.text)


class Boards(TrelloApi):
    """Representative of boards api"""

    def __init__(self):
        super().__init__()
        self._url = TrelloApi.API_URL + '/boards'

    def get_lists(self, id, fields=None):
        return super().get('{}/{}/lists'.format(self._url, id), params={'fields': fields})


class labels(TrelloApi):
    """Representative of labels api"""

    def __init__(self):
        super().__init__()
        self._url = TrelloApi.API_URL + '/labels'

    def get(self, id, fields=None):
        return super().get('{}/'.format(id), params={'fields': fields})


class Cards(TrelloApi):
    """Representative of cards api"""

    def __init__(self):
        super().__init__()
        self._url = TrelloApi.API_URL + '/cards'

    def add(self, id_list):
        return super().add(self._url, data={'idList': id_list})

    def add_label_to(self, card_id, name, color):
        return super().add('{}/{}/labels'.format(self._url, card_id), data={'color': color, 'name': name})

    def add_comment_to(self, card_id, comment):
        return super().add('{}/{}/actions/comments'.format(self._url, card_id), data={'text': comment})


def parse_args():
    parser = argparse.ArgumentParser(
        description='add a card to a given list in a board of trello.com')
    parser.add_argument(
        '-l', '--idList', help='id of a list in which the card is added', required=True)
    parser.add_argument(
        '-b', '--idBoard', help='id of a board containing the given list', required=True)
    parser.add_argument('-c', '--comment',
                        help='comment to be added to the card', required=False)
    parser.add_argument(
        '-t', '--labels', help='space separated list of labels to be added to the card (name:color name:color ...)', nargs='*', required=False)
    parser.add_argument('-d', '--debug', help='enable verbose output',
                        action='store_true', default=False)

    return parser.parse_args()


def is_list_in_board(id_list, id_board):
    """Check whether the given list is in the board or not """
    boards = Boards()
    list_ids = boards.get_lists(id_board, fields='id')
    logging.debug('boards:{} has lists:{}'.format(id_board, list_ids))

    for l in list_ids:
        if l['id'] == id_list:
            return True

    return False


def main():
    args = parse_args()

    log_level = logging.DEBUG if args.debug else logging.INFO
    logging.basicConfig(level=log_level)

    logging.debug('args: {}'.format(args))

    id_board = args.idBoard
    id_list = args.idList
    card_comment = args.comment
    labels = args.labels

    try:
        if not is_list_in_board(id_list, id_board):
            logging.error(
                'list:{} is not in board:{}'.format(id_list, id_board))
            sys.exit(1)

        cards = Cards()
        c = cards.add(id_list)
        logging.info('Added card:{} to list:{} in board:{}'.format(
            c['id'], c['idList'], c['idBoard']))

        cards.add_comment_to(c['id'], card_comment)
        logging.info('Added comment:"{}" to card:{}'.format(
            card_comment, c['id']))

        for label in labels:
            l = label.split(':')
            if len(l) != 2:
                logging.error('Invalid label:{} is provided.'.format(label))
            name, color = l[0], l[1]
            cards.add_label_to(c['id'], name, color)
            logging.info('Added {} to card:{}'.format(label, c['id']))

    except requests.HTTPError as http_err:
        logging.debug(http_err)
        logging.error('trello error: {}'.format(http_err.response.text))


if __name__ == "__main__":
    main()
