## Unit test
I have not added any unittest as almost all the functionaly is just call to trello API.

## Run in local machine
```bash

$> virtualenv -p python3 venv
$> source venv/bin/activate
$> pip install -r requirments.txt
$> export API_KEY=YOUR_API_KEY
$> export API_TOKEN=YOUR_API_TOKEN
$> python main.py -h
usage: main.py [-h] -l IDLIST -b IDBOARD [-c COMMENT]
               [-t [LABELS [LABELS ...]]] [-d]

add a card to a given list in a board of trello.com

optional arguments:
  -h, --help            show this help message and exit
  -l IDLIST, --idList IDLIST
                        id of a list in which the card is added
  -b IDBOARD, --idBoard IDBOARD
                        id of a board containing the given list
  -c COMMENT, --comment COMMENT
                        comment to be added to the card
  -t [LABELS [LABELS ...]], --labels [LABELS [LABELS ...]]
                        space separated list of labels to be added to the card
                        (name:color name:color ...)
  -d, --debug           enable verbose output

$> python main.py \ 
    -l aabbcc112233 \ 
    -b ddeeff445566 \
    -c 'This is a sample comment!' \
    -t label_1:red label_2:blue label_3:green
```

## Run via docker

```bash
$> docker build -t trello .
$> docker run -ti --rm \
    -e API_KEY=YOUR_API_KEY \
    -e API_TOKEN=YOUR_API_TOKEN \
    trello \
    -l aabbcc112233 \ 
    -b ddeeff445566 \
    -c 'This is a sample comment!' \
    -t label_1:red label_2:blue label_3:green
```